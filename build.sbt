name := "cache-loader"
version := "1.0"
scalaVersion := "2.11.8"

// Dependencies
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.6"
coverageEnabled := true

