ARTIFACTORY_URL=$2

# Get artifactory admin password without eching to screeen
read -s -p "Artifactory Password: " ARTIFACTORY_PWD
echo

tar -xvzf $1

ORIG_DIR=${PWD}

for dir in repo-archive/*/
do
    dir=${dir%*/}
    REPO_NAME=${dir##*/}
    echo uploading ${REPO_NAME}
    cd $dir && tar -zcf ../$REPO_NAME.tar.gz . && cd .. 
    jfrog rt u --explode --url $ARTIFACTORY_URL --user admin --password ${ARTIFACTORY_PWD} ${REPO_NAME}.tar.gz /${REPO_NAME}/
    cd ${ORIG_DIR}
done



