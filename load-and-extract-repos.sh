REPOS=( "ivy-scalaSbt" "ivy-typesafe" "mvn-mvnorg" )
TMP_DIR=$(mktemp -d)
ORIG_DIR=${PWD}
ARTIFACTORY_URL=$1

# Get artifactory admin password without eching to screeen
read -s -p "Artifactory Password: " ARTIFACTORY_PWD
echo 

# mv old ivy cache
echo "backing up existing ivy cahce"
mv ~/.ivy2 ~/.ivy2.orig

# Fetch sbt deps to prime cache
echo "Loading cache via sbt"
sbt update

# Extract evrything from the repos and tar it up
cd ${TMP_DIR}
mkdir repo-archive
cd repo-archive
for REPO in "${REPOS[@]}"
do
    echo "archiving ${REPO}"
    CACHE_REPO=${REPO}-cache
    mkdir ${REPO}
    jfrog rt dl /${CACHE_REPO}/ ${REPO}/ --url $ARTIFACTORY_URL --user admin --password ${ARTIFACTORY_PWD}
done

# Make one big tar of all the individual repos
echo "Combining arhives"
cd ${TMP_DIR}
tar -zcvf ${ORIG_DIR}/repo-archive.gz repo-archive
cd ${ORIG_DIR}

# Clean up after ourselves
echo "Cleaning up"
rm -rf ${TMP_DIR}

# restore old ivy cache
rm -rf ~/.ivy2
mv ~/.ivy2.orig ~/.ivy2
echo "Done!"
