package fish


/**
  * Created by chris on 04/03/2017.
  */
object Adder {

  def add(a: Int, b: Int) = a + b

  def doubleAdd(a: Int, b: Int) = a + b

}
