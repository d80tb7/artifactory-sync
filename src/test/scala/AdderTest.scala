import fish.Adder
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by chris on 04/03/2017.
  */
class AdderTest extends FlatSpec with Matchers {

  "Add" should "work" in {
    Adder.add(1, 1) should be (2)
  }
}
